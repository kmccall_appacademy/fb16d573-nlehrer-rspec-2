def measure(rep = 1)
  start = Time.now
  rep.times { yield }
  finish = Time.now
  (finish - start) / rep
end
